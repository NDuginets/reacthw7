import React, { Component } from 'react';
import Trubl from './Trubl.js'
import {Link} from 'react-router-dom';
import Cancel from './cancel.svg'
import Pagination from "react-js-pagination";
import PropTypes from 'prop-types';
import './App.css';
import Filter from './Filter.js'

class ListMovie extends Component {


HandletOnChange = (pageNumber) => {
  this.props.getListMovie(pageNumber)
}

componentDidMount() {
  this.props.getListMovie(this.props.ListMovie.page)
}
    render () {
     let {data, loaded} = this.props.ListMovie;
      if (loaded === true) {

          if (this.props.location.state !== undefined) {
             // Filter(this.props.location.state, data)
              let {filter} = this.props.location.state;

            if (filter === 'name') {
                data.sort(function (a, b){
                  if (a.title > b.title) {
                    return 1;
                  }
                  if (a.title < b.title) {
                    return -1
                  }
                  return 0
                })
              }

              if (filter === 'raiting') {
                data.sort(function (a, b)
                  {
                    return b.vote_average - a.vote_average;
                  })
                } 

              if (filter === 'runtime') {
                data.sort(function (a, b)
                  {
                    return b.popularity - a.popularity;
                  })
              } 
                console.log(data)
          }

        return(
          <div>
            <div className="row">
              {data.map(item => (
                  <div key={item.id} className="col s4">
                    <div className="card hoverable">
                      <div className="card-image waves-effect waves-block waves-light">
                          <img className="activator" src={item.backdrop_path === null ? './no_image.png' : `https://image.tmdb.org/t/p/w500/${item.backdrop_path}`} alt="img_movie"/>
                          <span className="flow-text">{item.title}</span>
                      </div>
                      <div className="card-reveal"> 
                          <div className="card-title grey-text text-darken-4">{item.title} <img className=" card-title B right material-icons" src={Cancel} alt="loading"/></div>                       
                          <p>{item.overview}</p>
                      </div>
                      <div className="card-action A">
                          <Link   to={`/movie/${item.id}`}>More</Link>
                      </div>
                    </div>
                </div>))}  
            </div>
              <Pagination 
              innerClass={"pagination"}
              activeClass={"active"}
              itemClass={"waves-effect"}
              activePage={this.props.ListMovie.page}
              itemsCountPerPage={1}
              totalItemsCount={this.props.ListMovie.totalpages}
              pageRangeDisplayed={5}
              onChange={this.HandletOnChange}
              />
            </div>
        );
      }
   else return ( <Trubl/>)
     }
    };

ListMovie.propTypes = {
  ListMovie: PropTypes.object,
  getListMovie: PropTypes.func,
}   

export default ListMovie;



//<Link key={item.id} to={`/movie/${item.id}`}>  {item.title}</Link> {item.overview} <img className="B right material-icons" src={Cancel} alt="loading"/>