import React, { Component } from 'react';
import Trubl from './Trubl.js'
import ReactStars from 'react-stars'
import PropTypes from 'prop-types';


class Movie extends Component {


componentDidMount() {
    this.props.getMovie(this.props.id)
}    

render() { 
    let {data, loaded} = this.props.Movie
    if (loaded === true) {
          return (     
            <div className="row flow-text">
                    <div className="col s12 flow-text center"><h2>{data.title}</h2></div>
                    <div className="col s3 flow-text center">
                        <div className="card">
                            <div className="c card-image">
                            <img  src={`https://image.tmdb.org/t/p/w500/${data.poster_path}` } alt="img_movie"/>
                            </div>
                        </div>
                    </div>
                <div className="col s9">
                    <div className="row flow-text">
                        <div className="col s6 flow-text">
                            <div className="card-panel">
                            <p>Название: {data.title}</p>
                            <p>Оригинальное название: {data.original_title}</p>
                            <p>Слоган: {data.tagline}</p>
                            <p>Бюджет: {data.budget}$</p>
                            <p>Дата релиза: {data.release_date}</p>
                            <p>Продолжительность: {data.runtime} мин.</p>
                            </div>
                        </div>
                
                    <div className="col s6 flow-text">
                        <div className="row flow-text">
                            <div className="col s12 flow-text">
                                <div className="card-panel">
                                    <div>Жанр: {data.genres.map((item,index) => ( <div key={index} className="chip"> {item.name} </div>))}</div>
                                    <div>Компании производители: {data.production_companies.map((item,index) => ( <div key={index} className="chip"> {item.name} </div>))}</div>
                                    <div>Страна: {data.production_countries.map((item,index) => ( <div key={index} className="chip"> {item.name} </div>))}</div>
                                </div>
                            </div>
                            <div className="col s12 flow-text">
                                <div className="card-panel">
                                    <div>Рейтинг: <ReactStars count={10} value={data.vote_average}  edit={false} size={28} color2={'#ffd700'} /> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col s12 flow-text">
                        <div className="card-panel">
                            <p>{data.overview}</p>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        
          )
        }  else return ( <Trubl/>)
    }
}

Movie.propTypes  = {
    Movie: PropTypes.object,
    getMovie: PropTypes.func,
    id: PropTypes.string
}

export default Movie;


/*<div className="col s3">
<div class="card-panel flow-text">
{data.overview}
</div>
</div>*/