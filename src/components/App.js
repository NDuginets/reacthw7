import React, { Component } from 'react';
import './App.css';
import MyListMovie from '../containers/ListMovieContainer'
import Header from './Header.js';
import Movie from '../containers/MovieContainer';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;



class App extends Component {
  render() {
    return (
      <div className="App">
        
        <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
        <div>
        <Header />   
        <Switch>
        <Route path="/movie" exact render={(props) => {             
              return <MyListMovie {...props}/>
          }
          } />
          

        <Route path="/movie/:id" exact render={({match}) => {             
              return <Movie id={match.params.id}/>
          }
          } /> 
        
        <Redirect from='/' to='/movie'/>             
      </Switch>
      </div>
    </BrowserRouter>      
      </div>
    );
  }
}

export default App;
