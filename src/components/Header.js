import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';



class Header extends Component {
    render() {
          return ( 
             
                  <nav className="nav-wrapper"> 
                        <ul className="left">
                              <li><NavLink  to={{ pathname: '/movie', state: { filter: 'name' }}}>По названию</NavLink></li>
                              <li> <NavLink  to={{ pathname: '/movie', state: { filter: 'raiting' }}}>По рейтингу</NavLink></li>
                              <li> <NavLink  to={{ pathname: '/movie', state: { filter: 'runtime' }}}>По популярности</NavLink></li>
                        </ul>
                  </nav>
          )
    }
}


export default Header;









