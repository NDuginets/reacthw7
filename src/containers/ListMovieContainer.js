import ListMovie from '../components/ListMovie';
import { connect } from 'react-redux'
import ActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
      ListMovie: state.ListMovie
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getListMovie: (page) => {
      dispatch( ActionCreator.getListMovie(page) );
    }
  };
};

const MyListMovie = connect(
  mapStateToProps,
  mapDispatchToProps
)(ListMovie);


export default MyListMovie;
