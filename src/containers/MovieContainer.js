import Movie from '../components/Movie';
import { connect } from 'react-redux'
import ActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
      Movie: state.Movie
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getMovie: (id) => {
      dispatch( ActionCreator.getMovie(id) );
    }
  };
};

const MyMovie = connect(
  mapStateToProps,
  mapDispatchToProps
)(Movie);


export default MyMovie;
