import axios from 'axios';

const key = "api_key=d5e6abc19b7394d6334f8b8f747988c9";


const ActionCreator = {

  getListMovie: (page) => {
    return function (dispatch, getState) {
      dispatch({
        type: "REQUEST_LIST"
      });
      axios.get(`https://api.themoviedb.org/3/discover/movie?${key}&primary_release_date.gte=2017-01-01&primary_release_date.lte=2017-12-31&page=${page}&language=ru`)
      .then(function(response) {
          console.log(response.data);
          dispatch({
            type: "RESPONSE_LIST",
            data: response.data
          });
      });
    };
  },



  getMovie: (id) => {
    return function (dispatch, getState) {
      dispatch({
        type: "REQUEST_MOVIE"
      });
      axios.get(`https://api.themoviedb.org/3/movie/${id}?${key}&language=ru`)
      .then(function(response) {
          console.log(response.data);
          dispatch({
            type: "RESPONSE_MOVIE",
            data: response.data
          });
      });
    };
  }

}

export default ActionCreator;
